//core.js
//====================================================
// Controlador 

angular.module('MainApp', [])


function mainController($scope, $http) {
	$scope.newRadio = {};
	$scope.radios   = {};
	$scope.selected = false;
	var audioActual = "";

	// Obtenemos todos los datos de la base de datos ========================
	$http.get('/api/radio').success(function(data) {
		$scope.radios = data;
	})
	.error(function(data) {
		console.log('Error: ' + data);
	});


	// Función para registrar a una radio ==================================
	$scope.registrarRadio = function() {
		$http.post('/api/radio', $scope.newRadio)
		.success(function(data) {
				$scope.newRadio = {}; // Borramos los datos del formulario
				$scope.radios   = data;
			})
		.error(function(data) {
			console.log('Error: ' + data);
		});
	};

	// Función para editar los datos de una radio ===========================
	$scope.modificarRadio = function(newRadio) {
		$http.put('/api/radio/' + $scope.newRadio._id, $scope.newRadio)
		.success(function(data) {
				$scope.newRadio = {}; // Borramos los datos del formulario
				$scope.radios   = data;
				$scope.selected = false;
			})
		.error(function(data) {
			console.log('Error: ' + data);
		});
	};

	// Función que borra un objeto radio conocido su id ======================
	$scope.borrarRadio = function(newRadio) {
		$http.delete('/api/radio/' + $scope.newRadio._id)
		.success(function(data) {
			$scope.newRadio = {}; // Borramos los datos del formulario
			$scope.radios   = data;
			$scope.selected = false;
		})
		.error(function(data) {
			console.log('Error: ' + data);
		});
	};

	// Función para coger el objeto seleccionado en la tabla ===================
	$scope.selectPerson = function(radio) {
		$scope.newRadio = radio;
		$scope.selected = true;
		console.log($scope.newRadio, $scope.selected);
	};

	$scope.selectRadioListen = function(radio) {
		// Limitar la cantidad de usuarios que escuchen una estacion
		
		if (radio.actualUsers < radio.maxUsers) {
			//alert("Cantidad de usuarios actuales es menor que usuarios maximos")
			
			//no funciona - controlar audio 
			// alert(audioActual);
			// radio.actualUsers = radio.actualUsers + 1;
			// var nombre_stream = radio.direccion + ";stream.mp3";			

			// if(audioActual == nombre_stream) {
  	// 			audioActual.pause(); 
 		// 	}

 		// 	if (nombre_stream.paused) {
 		// 		nombre_stream.play();
 		// 	} else {
   //  			nombre_stream.pause();
 		// 	}

 		// 	audioActual = nombre_stream
 		// 	var audio = new Audio(audioActual);
			// audio.play()

			//funciona
			radio.actualUsers = radio.actualUsers + 1;
			var nombrestream  = radio.direccion + ";stream.mp3";
			var audio         = new Audio(nombrestream);
			audio.play();
		} else if(radio.actualUsers == radio.maxUsers) {
			alert("Se ha alcanzado el maximo de usuarios en esta estacion, seleccione otra")
		} 
		
	};

}