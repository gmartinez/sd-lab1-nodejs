//radio.js
//==============
//	Se define la clase radio con sus atributos

var mongoose = require('mongoose');

module.exports = mongoose.model('Radio', {
	nombre: String,
	direccion: String,
	maxUsers: Number,
	actualUsers: Number
});