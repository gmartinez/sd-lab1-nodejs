//persona.js
//==============
//	Se define la clase persona con sus atributos

var mongoose = require('mongoose');

module.exports = mongoose.model('Persona', {
	nombre: String,
	apellido: String,
	edad: String
});