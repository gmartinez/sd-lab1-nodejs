//controller.js
//================================
// Controlador 



// Llamada a la clase ========================================================

//var Radio = require('./modelo/radio');  
var Radio = require('./modelo/radio');  


// Obtiene todos los objetos Radio de la base de datos =====================

exports.getRadio = function (req, res){
	Radio.find(
		function(err, radio) {
			if (err)
				res.send(err)
					res.json(radio); // devuelve todas las Radios en JSON		
				}
			);
}


// Guarda un objeto Radio en base de datos ==================================

exports.setRadio = function(req, res) {

		// Creo el objeto Radio
		Radio.create(
			{nombre : req.body.nombre,	
		     direccion: req.body.direccion, 
		     maxUsers: req.body.maxUsers, 
		     actualUsers: 0}, 
			function(err, radio) {
				if (err)
					res.send(err);

				// Obtine y devuelve todas las radios tras crear una de ellas
				Radio.find(function(err, radio) {
				 	if (err)
				 		res.send(err)
				 	res.json(radio);
				});
			});

	}


// Modificamos un objeto Radio de la base de datos =============================

exports.updateRadio = function(req, res){
	Radio.update( {_id : req.params.radio_id},
					{$set:{nombre : req.body.nombre,	
						   direccion: req.body.direccion, 
						   maxUsers: req.body.maxUsers,
						   actualUsers: 0}}, 
					function(err, radio) {
						if (err)
							res.send(err);

				// Obtine y devuelve todas las radios tras crear una de ellas
				Radio.find(function(err, radio) {
				 	if (err)
				 		res.send(err)
				 	res.json(radio);
				});
			});
	}


// Elimino un objeto Radio de la base de Datos ==================================

exports.removeRadio = function(req, res) {
	Radio.remove({_id : req.params.radio_id}, function(err, radio) {
		if (err)
			res.send(err);

			// Obtiene y devuelve todas las radios tras borrar una de ellas
			Radio.find(function(err, radio) {
				if (err)
					res.send(err)
				res.json(radio);
			});
		});
}