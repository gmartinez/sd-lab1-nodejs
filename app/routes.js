//routes.js
//=======================================
// Se define los get y post


//var Radio = require('./modelo/radio');
var Radio = require('./modelo/radio');
var Controller = require ('./controller');


module.exports = function(app) {

	// Devolver todos los Radios
	app.get('/api/radio', Controller.getRadio);
	
	// Crear una nueva Radio
	app.post('/api/radio', Controller.setRadio);
	
	// Modificar los datos de una Radio
	app.put('/api/radio/:radio_id', Controller.updateRadio);
	
	// Borrar una Radio
	app.delete('/api/radio/:radio_id', Controller.removeRadio);

	// application -------------------------------------------------------------
	app.get('*', function(req, res) {
		res.sendfile('./angular/index.html'); // Carga única de la vista
	});
};