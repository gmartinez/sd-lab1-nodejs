# Lab 1 SD #


## Lenguajes ocupados ##

* NodeJS 
* AngularJS
* Javascript
* HTML5


## Base de datos ocupada ##

* MongoDB


## Complementos de NodeJS Ocupados ##

* Moongose
* Socket.io

## Tolerancia de fallos ##

1. Forever: https://github.com/foreverjs/forever
* Instalar con sudo npm install forever -g
* Ejecutar app con: forever start server.js
* Detener app con: forever stop server.js
* Para saber si esta funcionando: ps aux | grep server.js