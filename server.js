//server.js
//================


// Inicializacion =======================================================================

var express  = require('express');
var app      = express(); 							// Utilizamos express
var mongoose = require('mongoose'); 				// mongoose para mongodb
var port  	 = process.env.PORT || 8080; 			// Puerto 8080

var http   = require('http');
var server = http.createServer(app);
var io     = require('socket.io').listen(server);

server.listen(3001);
console.log("Server corriendo en el puerto:  " + 3001);

// Configuracion ========================================================================

var uri = "mongodb://localhost:27017/radio-usach";
mongoose.connect(uri); 	// Hacemos la conexion a la base de datos de Mongo con nombre radio-usach
console.log("Conectado a MongoDB exitosamente en: " + uri)

app.configure(function() {
	app.use(express.static(__dirname + '/angular')); 		
	app.use(express.logger('dev')); 						
	app.use(express.bodyParser());
	app.use(express.methodOverride());
});


// Cargamos los endpoints donde estan get y post =======================================

require('./app/routes.js')(app);

// Puerto escucha ======================================================================

//app
//app.listen(port);
//console.log("App corriendo en el puerto:  " + port);

// Socket.io ===========================================================================


//variable, que almacena, los nombres de usuario que estan conectados al chat actualmente.
var usernames = {};

// arreglo de las Salas que estan disponibles.
var rooms = ['Sala1','Sala2','Sala3'];

io.sockets.on('connection', function (socket) {
	
	//cuando se emite un "adduser", el cliente emite y escucha
	socket.on('adduser', function(username){
		// almacena el nombre de usuario en el socket 
		socket.username = username;
		// almacena la Sala  en la que ingresa el usuario
		socket.room = 'Sala1';
		// agrega el nombre usuario al a variable global que almacena los nombres de usuarios
		usernames[username] = username;
		// se envia al usuario a la Sala 
		socket.join('Sala1');
		//Se ha conectado al a la sala 
		socket.emit('updatechat', 'SERVER', 'te has conectado a la  Sala1');
		// Emite un mensaje indicandole al usuario que se ha conectado a la Sala
		socket.broadcast.to('Sala1').emit('updatechat', 'SERVER', username + ' se ha conectado a esta Sala');
		socket.emit('updaterooms', rooms, 'Sala1');
	});
	
	// el usuario cuando emite un mensaje, se escucha y es esuchado
	socket.on('sendchat', function (data) {
		// we tell the client to execute 'updatechat' with 2 parameters
		io.sockets.in(socket.room).emit('updatechat', socket.username, data);
	});
	
	socket.on('switchRoom', function(newroom){
		socket.leave(socket.room);
		socket.join(newroom);
		socket.emit('updatechat', 'SERVER', 'te has conectado a '+ newroom);
		// se envia un mensaje a la sala anterior indicando la desconeccion de esa sala
		socket.broadcast.to(socket.room).emit('updatechat', 'SERVER', socket.username+' se ha ido de la Sala');
		// al socket se actualiza el nombre-titulo de la Sala
		socket.room = newroom;
		socket.broadcast.to(newroom).emit('updatechat', 'SERVER', socket.username+' se ha unido a esta Sala');
		socket.emit('updaterooms', rooms, newroom);
	});
	
	// cuando el usuario se desconecta
	socket.on('disconnect', function(){
		// se remueve el el nombre de la variable global Username
		delete usernames[socket.username];
		// Se actualiza los usuarios que estan dentro del chat
		io.sockets.emit('updateusers', usernames);
		// se indica gobalmente que el usuario se ha desconectado 
		socket.broadcast.emit('updatechat', 'SERVER', socket.username + ' se ha desconectado del Chat');
		socket.leave(socket.room);
	});
}); // fin io.socket.on
